package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DivisionTest {


    @Test
    public void testCntBNotZero() throws Exception {
        int result = new Division().division(2,1);
        assertEquals(2, result);
    }

    @Test
    public void testCntBIsZero() throws Exception {
        int result = new Division().division(2,0);
        assertEquals(  Integer.MAX_VALUE, result);
    }
}
